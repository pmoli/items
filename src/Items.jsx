import React, { useState, useEffect} from "react";
import { v4 as uuid } from "uuid";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Col, Badge, Row, ListGroup, ListGroupItem, Input, Button } from 'reactstrap';

const Items = () => {
  const [items, setItems] = useState([]);
  const [url, setUrl] = useState();

  useEffect(()=>{
    console.log('ola')
    recuperar()
  },[])

  useEffect(()=>{  //para guardar automaticamente cada vez que haya un cambio en los items(añadir foto o like)
    if(items.length){ //para asegurarse de que no nos guarda un array vacio antes de cargar los items en e state
      guardar()
    }
  },[items])

  const guardar = () => {
    localStorage.setItem('mis_items', JSON.stringify(items));
  }

  const recuperar = () => {
    const itemsJson = localStorage.getItem('mis_items');
    const cosas = JSON.parse(itemsJson);
    if (cosas && cosas.length) {
      setItems(cosas);
    } else {
      setItems([]);
    }

  }

  const afegir = () => {
    if (url) {
      const nouItem = {
        imagen: url,
        id: uuid(),
      };
      setItems([...items, nouItem]);
      setUrl('');
    }

  };

  const tots = items.map((el) => (
    <Col xs='12' md='6' lg='4'>
      <ListGroup key={el.id}>
        <ListGroupItem>
          Subido por {el.id.slice(0, 5)}
        </ListGroupItem>
        <img className='img-fluid' src={el.imagen} />
        <ListGroupItem>
          <i style={{ color: 'grey' }} className="fa fa-thumbs-up"></i> <Badge pill>14</Badge>
        </ListGroupItem>
      </ListGroup>
    </Col>
  ));

  return (
    <>
      <Input style={{ width: '400px' }} type="text" value={url} onChange={(ev) => setUrl(ev.target.value)} />
      <br />
      <Button color="danger" onClick={afegir}>Afegir</Button>
      <Button onClick={recuperar}>Leer datos</Button>
      <br />
      <br />

      <Container>
        <Row>
          {tots}
        </Row>
      </Container>
    </>
  );
};

export default Items;
